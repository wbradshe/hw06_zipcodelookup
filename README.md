Module 6 Homework

Due May 6 by 11:59pm Points 50 Submitting a file upload Available after Mar 7 at 12am

Do the zip code lookup script (Wicked Cool Shell Scripts P.182) as discussed in class.

Then complete the area code lookup script (Wicked Cool Shell Scripts P.182).

Submit both scripts here!
