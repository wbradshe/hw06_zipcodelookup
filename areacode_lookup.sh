#!/bin/bash
#
#Algorithm
#
#Find out the area code to search
#
#insert area code in URL
#
#Get the city name from web page data
#
#display City and State
##########################################################
#Pseudo Code
#
#
#read "user prompt" var
#
#areaCode=$(curl URL | grep something | cut somethin)
#
#echo areaCode
#
# 
###############################################################
#URL to use: http://www.bennetyee.org/ucsd-pages/area.html
#
#
#731 for testing. Memphis, Tennessee
#
read -p "Please enter a 3 digit area code: " tempArea


#grep for area code info curl -s https://www.bennetyee.org/ucsd-pages/area.html | grep -P ">731<" | grep -P "td>   \w[.,?:\w\d ]+"


City_State=$(curl -s https://www.bennetyee.org/ucsd-pages/area.html | grep -P ">$tempArea<" | grep -Po "td>   \w[.,?:\w\d ]+" | cut -d ">" -f2 | head -n1)

echo "$tempArea is the Area Code for: $City_State"
